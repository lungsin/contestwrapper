from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from getpass import getpass


class TLX:
    def __init__(self):
        self.driver = webdriver.Chrome()
        
    def login(self):
        self.driver.get("https://tlx.toki.id/login")
        user_box = driver.find_element_by_name("usernameOrEmail")
        pass_box = driver.find_element_by_name("password")
        user_box.send_keys(getuser())
        pass_box.send_keys(getpass())
        pass_box.submit()
        
    def submit(self, url, file_loc, language):
        self.driver.get(url)
        upl_file = self.driver.find_element_by_name('source')
        select_lang = Select(self.driver.find_element_by_name('language'))
        
        upl_file.send_keys(file_loc)
        select_lang.select_by_value(language)
        
    def logout(self):
        self.driver.quit()
    